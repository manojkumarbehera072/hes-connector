﻿using System;
using System.Collections.Generic;

namespace TataPower.UDS.HES.Connector.Models
{
    public class EquipmentActivityInput
    {
        public UpdateMessageHeader UpdateMessageHeader { get; set; }
        public UpdateMessageBody UpdateMessageBody { get; set; }
    }
    public class UpdateMessageHeader
    {
        public string MessageID { get; set; }
        public DateTime DateTime { get; set; }
    }

    public class UpdateEquipment
    {
        public string RefID { get; set; }
        public string EquipmentID { get; set; }
        public DateTime InstallationDate { get; set; }
        public DateTime RemovalDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ConsumerID { get; set; }
        public string OfficeID { get; set; }
        public string ConnStatus { get; set; }
        public string ConsumerType { get; set; }      
    }

    public class UpdateMessageBody
    {
        public List<UpdateEquipment> UpdateEquipment { get; set; }
    }
}
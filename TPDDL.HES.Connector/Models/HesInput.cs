﻿namespace TataPower.UDS.HES.Connector.Models
{
    public class HesInput
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string DeviceID { get; set; }
        public string ReplyURL { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace TataPower.UDS.HES.Connector.Models
{
    public class EquipmentInventoryInput
    {
        public MessageHeader MessageHeader { get; set; }
        public MessageBody MessageBody { get; set; }
    }
    public class MessageHeader
    {
        public string MessageID { get; set; }
        public DateTime DateTime { get; set; }
    }

    public class Equipment
    {
        public string RefID { get; set; }
        public string EquipmentID { get; set; }
        public string AltequipmentID { get; set; }
        public string EquipmentMake { get; set; }
        public string EquipmentMaterialNumber { get; set; }
        public string EquipmentType { get; set; }
        public string SimSerialNumber { get; set; }
        public string NicSerialNumber { get; set; }
        public string SimMobileNo { get; set; }
        public string SimIPAddress { get; set; }
        public string MeteringMode { get; set; }
    }

    public class MessageBody
    {
        public List<Equipment> Equipment { get; set; }
    }
}
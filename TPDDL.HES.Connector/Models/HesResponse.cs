﻿namespace TataPower.UDS.HES.Connector.Models
{
    public class HesResponse
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
}
﻿using log4net;
using log4net.Repository;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using TataPower.UDS.HES.Connector.Helper;
using TataPower.UDS.HES.Connector.Models;

namespace TataPower.UDS.HES.Connector
{
    /// <summary>
    /// Summary description for TataPowerUDSHESConnectorService
    /// </summary>
    [WebService(Namespace = "http://tatapower-uds.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TataPowerUDSHESConnectorService : System.Web.Services.WebService
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        HES_Helper hes = new HES_Helper();

        [WebMethod]
        public HesResponse Disconnect(HesInput hesInput)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs

                HesResponse response = new HesResponse();
                response.Action = hesInput.Action;
                response.Status = "Success";
                response.Code = 0;
                response.RefID = hesInput.RefID;
                return response;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :Disconnect --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [WebMethod]
        public HesResponse Connect(HesInput hesInput)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs

                HesResponse response = new HesResponse();
                response.Action = hesInput.Action;
                response.Status = "Success";
                response.RefID = hesInput.RefID;
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :Connect --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [WebMethod]
        public HesResponse Ping(HesInput hesInput)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs

                HesResponse response = new HesResponse();
                response.Action = hesInput.Action;
                response.Status = "Success";
                response.Code = 0;
                response.RefID = hesInput.RefID;
                return response;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :Ping --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [WebMethod]
        public HesResponse RelayStatus(HesInput hesInput)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs

                HesResponse response = new HesResponse();
                response.Action = hesInput.Action;
                response.Status = "Success";
                response.RefID = hesInput.RefID;
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :RelayStatus --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [WebMethod]
        public HesResponse ODR(ODRInput odrInput)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs

                HesResponse response = new HesResponse();
                response.Action = odrInput.Action;
                response.Status = "Success";
                response.RefID = odrInput.RefID;
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :ODR --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [WebMethod]
        public HesResponse DemandReset(DemandResetInput demandResetInput)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs

                HesResponse response = new HesResponse();
                response.Action = demandResetInput.Action;
                response.Status = "Success";
                response.RefID = demandResetInput.RefID;
                response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :DemandReset --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [WebMethod]
        public HesResponse BalanceUpdate(BalanceUpdateInput balanceUpdateInput) //New Input Model   BalanceUpdateInput
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs
                HesResponse response = new HesResponse();
                //response.Action = balanceUpdateInput.Action;
                //response.Status = "Success";
                //response.RefID = balanceUpdateInput.RefID;
                //response.Code = 0;
                return response;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :BalanceUpdate --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }

        }

        [WebMethod]
        public HesResponse CreateEquipmentInventory(EquipmentInventoryInput equipmentInventoryInput) //New Input Model   BalanceUpdateInput
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs
                HesResponse response = new HesResponse();
                //response.Action = balanceUpdateInput.Action;
                response.Status = "Success";
                //response.RefID = balanceUpdateInput.RefID;
                response.Code = 0;
                return response;

                //Create Input Model
                //HES_Workflow status change
            }
            catch (Exception ex)
            {
                string errorLog = "Method :CreateEquipmentInventory --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [WebMethod]
        public HesResponse UpdateEquipmentActivity(EquipmentActivityInput equipmentActivityInput) //New Input Model   BalanceUpdateInput
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                //do stuffs
                HesResponse response = new HesResponse();
                //response.Action = balanceUpdateInput.Action;
                response.Status = "Success";
                //response.RefID = balanceUpdateInput.RefID;
                response.Code = 0;
                return response;

                //Create Input Model
                //HES_Workflow status change
            }
            catch (Exception ex)
            {
                string errorLog = "Method :UpdateEquipmentActivity --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                throw ex;
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void HESWorkFlowResponse(HESReplyResult hesReplyResult)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                string uuid = "";
                string createdBy = "";
                bool isRejected = false;
                string errorMessage = "";
                string referenceID = "";
                string updatefields = "";
                int retryCount = 0;
                ExceptionCISSync exceptionSync = new ExceptionCISSync();
                HES_Workflow hES_Workflow = new HES_Workflow();

                //Validation
                if (hesReplyResult != null)
                {
                    if (!String.IsNullOrEmpty(hesReplyResult.RefID))
                    {
                        referenceID = hesReplyResult.RefID;
                    }
                    if (!String.IsNullOrEmpty(hesReplyResult.ResponseText))
                    {
                        updatefields += "Workflow_Response_Text =" + "'" + hesReplyResult.ResponseText + "',";
                    }
                    if (hesReplyResult.HESResponseDateTime != null)
                    {
                        updatefields += "Response_Received_Time =" + "'" + hesReplyResult.HESResponseDateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                    }
                    //Update Workflow_Status to 3 or 4 based on ResponseCode 0=3 or 1=4
                    if (!String.IsNullOrEmpty(hesReplyResult.ResponseCode))
                    {
                        if (hesReplyResult.ResponseCode == "0")
                        {
                            updatefields += "Workflow_Status =" + "'3',";
                        }
                        else if (hesReplyResult.ResponseCode == "1")
                        {
                            updatefields += "Workflow_Status =" + "'4',";
                        }
                    }

                    updatefields += "Changed_By = 'HES_Connector', Changed_On = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                    retryCount = hES_Workflow.GetRetryCountByRefId(referenceID);
                    retryCount = retryCount + 1;
                    updatefields += "Retry_Count = " + retryCount + " ";

                    updatefields = updatefields.TrimEnd(',');
                }

                if (!isRejected)
                {
                    using (SqlConnection connection = new SqlConnection(constr))
                    {
                        connection.Open();
                        query = "update HES_WORKFLOW set " + updatefields + "where Id = '" + referenceID + "'";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                    //Write to log
                    var xmlString = Serialize<HESReplyResult>(hesReplyResult);
                    WriteToLog(xmlString, uuid, "~/CISAdapterLog/");
                }
                else
                {
                    var xmlStrings = Serialize<HESReplyResult>(hesReplyResult);
                    WriteToLog(xmlStrings, uuid, "~/CISAdapterLog/Rejected/");
                }

                //Exception_CIS_Sync
                errorMessage = errorMessage.TrimEnd(',');

                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = createdBy;
                exceptionSync.ServiceName = "HESWorkFlowResponse";
                Exception_HES_Connector exception_HES_Connector = new Exception_HES_Connector();
                exception_HES_Connector.Exception_HES_Connector_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :HESWorkFlowResponse --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
            }

        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void AMIDCTResponse(HESReplyResult hesReplyResult)
        {
            SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                string uuid = "";
                string createdBy = "";
                bool isRejected = false;
                string errorMessage = "";
                string referenceID = "";
                string updatefields = "";
                string responseStatus = "";
                ExceptionCISSync exceptionSync = new ExceptionCISSync();
                AMIDeviceControl aMIDeviceControl = new AMIDeviceControl();
                AMIDeviceControlModel aMIDeviceControlModel = new AMIDeviceControlModel();

                //Validation
                if (hesReplyResult != null)
                {
                    if (!String.IsNullOrEmpty(hesReplyResult.RefID))
                    {
                        referenceID = hesReplyResult.RefID;
                    }
                    if (!String.IsNullOrEmpty(hesReplyResult.ResponseCode))
                    {
                        updatefields += "Response_Code =" + "'" + hesReplyResult.ResponseCode + "',";
                    }
                    if (!String.IsNullOrEmpty(hesReplyResult.ResponseText))
                    {
                        updatefields += "Response_Text =" + "'" + hesReplyResult.ResponseText + "',";
                    }
                    if (!String.IsNullOrEmpty(hesReplyResult.ResponseStatus))
                    {
                        responseStatus = hesReplyResult.ResponseStatus;
                        updatefields += "Response_Status =" + "'" + responseStatus + "',";
                    }
                    if (hesReplyResult.AMIResponseDateTime != null)
                    {
                        updatefields += "AMI_Response_DateTime =" + "'" + hesReplyResult.AMIResponseDateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                    }
                    if (hesReplyResult.HESResponseDateTime != null)
                    {
                        updatefields += "HES_Response_DateTime =" + "'" + hesReplyResult.HESResponseDateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                    }
                    //Update HES_Ack_Status to 12 or 13 based on ResponseCode 0=12 or 1=13
                    if (!String.IsNullOrEmpty(hesReplyResult.ResponseCode))
                    {
                        if (hesReplyResult.ResponseCode == "0")
                        {
                            updatefields += "HES_Ack_Status =" + "'12',";
                        }
                        else if (hesReplyResult.ResponseCode == "1")
                        {
                            updatefields += "HES_Ack_Status =" + "'13',";
                        }
                    }

                    updatefields += "Changed_by = 'HES_Connector', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                    updatefields += "Request_Status =" + "'3',";

                    updatefields = updatefields.TrimEnd(',');
                }

                if (!isRejected)
                {
                    using (SqlConnection connection = new SqlConnection(constr))
                    {
                        connection.Open();
                        query = "update AMIDeviceControl set " + updatefields + "where Refrence_ID = '" + referenceID + "'";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                    //Write to log
                    var xmlString = Serialize<HESReplyResult>(hesReplyResult);
                    WriteToLog(xmlString, uuid, "~/CISAdapterLog/");
                }
                else
                {
                    var xmlStrings = Serialize<HESReplyResult>(hesReplyResult);
                    WriteToLog(xmlStrings, uuid, "~/CISAdapterLog/Rejected/");
                }
                aMIDeviceControlModel = aMIDeviceControl.GetAMIDeviceControlByReferenceID(referenceID);

                #region Confirmation_In
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_InBinding cls = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_InBinding();
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmrtMtrUtilsConncnStsChgReqERPCrteConfMsg req = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmrtMtrUtilsConncnStsChgReqERPCrteConfMsg();
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.BusinessDocumentMessageHeader head = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.BusinessDocumentMessageHeader();
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID uid = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID();
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID REFuid = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID();
                //SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfUtilsDvce devie = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfUtilsDvce();
                uid.Value = uuid;
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilitiesDeviceID deviceid = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilitiesDeviceID();
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.Log log = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.Log();
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem[] logitem = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem[1];
                SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem logitemr = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem();

                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TataPower-UDS";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                req.MessageHeader = head;
                req.UtilitiesConnectionStatusChangeRequest.ID.Value = referenceID;
                req.UtilitiesConnectionStatusChangeRequest.CategoryCode.Value = aMIDeviceControlModel.CategoryCode;
                foreach (var DeviceConnectionStatus in req.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus)
                {
                    DeviceConnectionStatus.ProcessingDateTime = aMIDeviceControlModel.ProcessingDateTime;
                    DeviceConnectionStatus.UtilitiesDeviceID.Value = aMIDeviceControlModel.UtilitiesDeviceID;
                }
                //deviceid.Value = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.ID.Value;
                //devie.ID = deviceid;
                //req.UtilitiesDevice = devie;
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "0000";
                logitemr.SeverityCode = "1";
                logitemr.Note = "Request Processed Successfully";
                logitem[0] = logitemr;
                log.Item = logitem;
                //req.Log = log;
                cls.SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In(req);
                #endregion

                //update Request_Status=4 in AMIDeviceControl table
                if (aMIDeviceControlModel.ID > 0)
                {
                    aMIDeviceControl.AMIDeviceControlRequestStatusUpdate(aMIDeviceControlModel.ID, 4, responseStatus);
                }
                //Exception_CIS_Sync
                errorMessage = errorMessage.TrimEnd(',');

                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = createdBy;
                exceptionSync.ServiceName = "AMIDCTResponse";
                Exception_HES_Connector exception_HES_Connector = new Exception_HES_Connector();
                exception_HES_Connector.Exception_HES_Connector_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :AMIDCTResponse --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
            }
        }

        private static string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void WriteToLog(string xmlStrings, string uuid, string path)
        {
            string fileNames = uuid + ".txt";
            string roots = System.Web.HttpContext.Current.Server.MapPath(@path);
            if (!Directory.Exists(roots))
            {
                Directory.CreateDirectory(roots);
            }
            string subdirs = roots + DateTime.Today.ToString("yyyy-MM-dd");
            if (!Directory.Exists(subdirs))
            {
                Directory.CreateDirectory(subdirs);
            }
            File.WriteAllText(subdirs + "/" + fileNames, xmlStrings);
        }

        private static void SetLogFileName(string name)
        {
            log4net.Repository.ILoggerRepository RootRep;
            RootRep = LogManager.GetRepository(Assembly.GetCallingAssembly());

            XmlElement section = ConfigurationManager.GetSection("log4net") as XmlElement;

            XPathNavigator navigator = section.CreateNavigator();
            XPathNodeIterator nodes = navigator.Select("appender/file");
            foreach (XPathNavigator appender in nodes)
            {
                appender.MoveToAttribute("value", string.Empty);
                appender.SetValue(string.Format(appender.Value, name + "+"));
            }

            IXmlRepositoryConfigurator xmlCon = RootRep as IXmlRepositoryConfigurator;
            xmlCon.Configure(section);
        }

    }
}

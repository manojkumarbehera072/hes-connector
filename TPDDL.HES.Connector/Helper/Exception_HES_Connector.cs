﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TataPower.UDS.HES.Connector.Models;

namespace TataPower.UDS.HES.Connector.Helper
{
    public class Exception_HES_Connector
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        HesConnectorLog hesConnectorLog = new HesConnectorLog();

        public void Exception_HES_Connector_Entry(ExceptionCISSync exceptionCISSync)
        {
            hesConnectorLog.SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";

            string fields = "";
            string values = "";

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    if (exceptionCISSync != null)
                    {
                        //validations
                        if (!String.IsNullOrEmpty(exceptionCISSync.UUID))
                        {
                            fields += "UUID,";
                            values += "'" + exceptionCISSync.UUID + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.Status))
                        {
                            fields += "Status,";
                            values += "'" + exceptionCISSync.Status + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.Message))
                        {
                            fields += "Message,";
                            values += "'" + exceptionCISSync.Message + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.CreatedBy))
                        {
                            fields += "Created_by,";
                            values += "'" + exceptionCISSync.CreatedBy + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.ServiceName))
                        {
                            fields += "Service_Name,";
                            values += "'" + exceptionCISSync.ServiceName + "',";
                        }
                        fields += "Created_on,";
                        values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');

                        connection.Open();
                        query = "insert into Exception_HES_Connector (" + fields + ") values(" + values + ")";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                string errorLog = "Method :Exception_HES_Connector_Entry --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}
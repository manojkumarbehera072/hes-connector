﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TataPower.UDS.HES.Connector.Helper
{
    public class HES_Workflow
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        HesConnectorLog hesConnectorLog = new HesConnectorLog();

        public int GetRetryCountByRefId(string referenceId)
        {
            hesConnectorLog.SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int retryCount = 0;
                    connection.Open();
                    query = "select Retry_Count from HES_WORKFLOW where Id='" + referenceId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        retryCount = reader["Retry_Count"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Retry_Count"].ToString());
                    }
                    connection.Close();
                    return retryCount;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetRetryCountByRefId --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}
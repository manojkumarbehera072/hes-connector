﻿using log4net;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TataPower.UDS.HES.Connector.Helper
{
    public class HES_Helper
    {

        SqlDataReader reader;
        SqlDataAdapter Adp;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        HesConnectorLog hesConnectorLog = new HesConnectorLog();

        public int GetHESIdByHESCode(string hesName)
        {
            hesConnectorLog.SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    query = "select id from hes_master where hes_cd='" + hesName + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["id"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetHESIdByHESCode --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public DataTable GetSAPAMIUrl(string servicename)
        {
            hesConnectorLog.SetLogFileName("Hes_Connector_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string query = "";
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    query = "select ServiceUrl,UserId,Password from SAP_AMI_ADAPTOR_URL where upper(ServiceName)=upper('" + servicename + "')";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    DataTable dturl = new DataTable();
                    dturl.Clear();
                    Adp = new SqlDataAdapter();
                    Adp.SelectCommand = cmd;
                    Adp.Fill(dturl);

                    connection.Close();
                    return dturl;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetSAPAMIUrl --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

    }
}